#!/usr/bin/node
{

const assert = require("assert");
const sleep = require("sleep");

const {Builder, By, Capability, until} = require("selenium-webdriver");
const sprintf = require( "sprintf-js" ).sprintf;
const fs = require("fs");

async function die(driver,msg,code){
	(( typeof code == "undefined" ) || !Number.isInteger( code )) && ( code = 1 );

	console.log("");
	console.log("*** error: " + msg );

	if( !!driver ){
		await driver.quit();
	}

	process.exit( code );
}

const args = process.argv.slice( 2 );

const scheme = args[0];
const site   = args[1];
const base   = args[2];
const bdir   = args[3]; // where to drop screenshots

!!scheme              || die( null, "[1] no scheme (like 'https')" );
!!site                || die( null, "[2] no site (like 'www')" );
!!base                || die( null, "[3] no base (like '.vonigo.com')" );
!!bdir                || die( null, "[4.1] no base directory where to drop the screenshots (like 'log')" );
fs.existsSync( bdir ) || die( null, "[4.2] base directory does not exist ( '"+bdir+"' )" );

const url = scheme + "://" + site + base;  // create url from command line parameters

async function takeScreenshot(driver,file){
	await driver.takeScreenshot().then((result)=>{
		fs.writeFile(file,result,"base64",(err)=>{
			!!err && die( driver, err );
		});
	});
}

function byType( target_type, target_string ){
	switch( target_type ){
		case "id":   return By.id   ( target_string );
		case "css":  return By.css  ( target_string );
		case "name": return By.name ( target_string );
		case "xpath":return By.xpath( target_string );
		default: 
			die( null, "cannot understand target type '"+target_type +"'." );
			return null;
	}
};

let warnings = "";
function warning( s ){
	warnings += ( "\n * " + s );
}
function printWarnings(){
	if( !warnings.length ){ return; }
	console.warn( "\nWarnings:" + warnings );
}

async function oneTest(u,t,d){
	let counter = 0; // count steps
	let driver = await new Builder().forBrowser("firefox").build();

	(( typeof t.commands == "undefined" ) || !Array.isArray( t.commands ))
		&& die( driver, "[5] cannot understand Selenium script" );

	for( let i=0 ; i < t.commands.length ; i++ ){
		const step = t.commands[i];
		const screenshot = sprintf( d + "/%03d.png", counter++ );

// 		console.log( step.command + " @ " + step.target + " / " + step.value );
		process.stdout.write( "." );

		if( step.command.search( /^\/\// ) >= 0 ) { continue; }   // skip commands that are commented out

		switch( step.command ){ // commands without a target
			case "open":
				await driver.get( u );
// 				takeScreenshot( driver, screenshot );
				continue;
			case "setWindowSize":
// 				warning( "setWindowSize is not supported" );
				continue;
			case "pause":
				sleep.sleep( step.value );
				continue;
		}

		const matches = RegExp(/([^=]+)=(.+)/).exec( step.target );

		( !matches || matches.length < 0 )
			&& await die( driver, "don't know how to process '"+step.command +"' without a target." );

		const target_type   = matches[1];
		const target_string = matches[2];

		const max_count = 10;
		let count = 0;
		let element = null;

		while( true ){
			const elements = await driver.findElements( await byType( target_type, target_string ));

			if(( typeof elements == "undefined" )|| !elements ) { continue; } // target not recognized

			if( !elements.length || !await elements[0].isDisplayed() || !await elements[0].isEnabled()){
				if( ++count > max_count ) {
					await die( driver, "[7] cannot interact with target '" + step.target + "'." );
				}
				process.stdout.write( "*" );
				await driver.sleep( 1000 );
				continue;
			}

			element = elements[0];
			break;
		}

		assert( typeof element != "undefined" );

		let fail = false; // whether an exception was encountered

		while( true ){
			fail = false;
			switch( step.command ){
				case "click":
					await takeScreenshot( driver, screenshot );
					try{
						await element.click();
					}catch( err ){
						if( ++count > max_count ) {
							await die( driver, "[9] cannot click target '" + step.target + "'." + "\n"+err);
						}
						process.stdout.write( "#" );
						await driver.sleep( 1000 );
						fail = true;
					}
					break;
				case "type":
					await element.sendKeys( step.value );
					break;
				case "select":
					const labels = RegExp(/label=(.+)/).exec( step.value );
					(!labels ||(labels.length < 2)) && die( "[8] cannot find label for select target '"+step.target+"'" );
					const el = await driver.findElement( byType(
						"xpath", "//select[\@id='"+target_string+"']/option[text()='"+labels[1]+"']")
					);
					await el.click();
					break;
				default: // not supported
					die( "[10] command '"+step.command +"' not supported." );
					break;
			}

			if( fail ){
				continue;
			}

			break;
		}

		await driver.sleep(100);
	}

	await driver.quit();
	printWarnings();

	console.log( "\nok" );
}

let f = ""; // contents read from input

require( "readline" ).createInterface({
	input : process.stdin
}).on( "line", (line) => { // read Selenium script from input
	f += line;
}).on( "close", async () => {
	const script = JSON.parse( f );

	(( typeof script.tests == "undefined" ) || !Array.isArray( script.tests )) &&
		die( null, "[6] cannot understand Selenium script" );

	await script.tests.forEach( async ( t )=>{
		(( typeof t.commands == "undefined" ) || !Array.isArray( t.commands )) &&
		die( null, "[7] cannot understand Selenium script (" + t.name + ")" );

		console.log( "running: "+site+"|"+t.name );

		const dir = bdir+"/"+Date.now()+"-"+site+"-"+t.name;

		fs.mkdirSync( dir );

// 		mkdir( $dir ) or die "*** error: cannot create directory '$timestamp'$/";

		await oneTest( url, t, dir ); // TODO: catch exceptions + terminate driver
	});

});

}
