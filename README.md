# qa-ui-auto

### What is this repository for? ###

* This is a repository for code related to the UI testing automation using Selenium.
* Two scripts that can run Selenium "side scripts" captured in a browser:
	* a perl script (celine)
	* a node.js script (celine.js): added following popular demand
* Three examples (run them like below):
	* a simple smoke test (smoke.side)
	* a warmup script (andyoncall, aussipetmobile)
	* an OBE testing script (obe-sirgrout

---
	cat smoke.side | node celine.js https apm .chief.vonigo.cool/external log
	cat aussiepetmobileusa.side | node celine.js https apm .chief.vonigo.cool log
	cat obe-sirgrout.side | node celine.js https sirgrout .chief.vonigo.cool/external log
---

* Log files (screenshots) for running the examples

### Who do I talk to? ###

* author: Manuel Zahariev (was: [mzahariev@vonigo.com](mailto:mzahariev@vonigo.com); now: [m@s2x.co](mailto:m@s2x.co) or [manuel@skwez.com](mailto:manuel@skwez.com))


	
